package com.omerfaruk.goodsfrom.dto;

public class AddBarcodeInput {
  private String barcode;

  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }
}
