package com.omerfaruk.goodsfrom.dto;

public class AddBarcodeOutput {
  private String message;

  public AddBarcodeOutput(String barcode) {
    this.message = barcode + " added succesfully!";
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
