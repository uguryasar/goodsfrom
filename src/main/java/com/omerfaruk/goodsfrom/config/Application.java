package com.omerfaruk.goodsfrom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.omerfaruk.goodsfrom")
public class Application {

  public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
    }

}
